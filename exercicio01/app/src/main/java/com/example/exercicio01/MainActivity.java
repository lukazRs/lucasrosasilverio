package com.example.exercicio01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    int valor;
    String digitado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        valor = new Random().nextInt();
    }

    public void ativarPalpite(View view) {
        EditText editText = (EditText)findViewById(R.id.editText);
        TextView textView = (TextView)findViewById(R.id.textView);
        digitado = editText.getText().toString();

        if(Integer.parseInt(digitado) == valor) {
            valor = new Random().nextInt();
            textView.setText(getResources().getString(R.string.acertou));
        } else if(Integer.parseInt(editText.getText().toString()) < valor) {
            textView.setText(getResources().getString(R.string.maior));
        } else if(Integer.parseInt(editText.getText().toString()) > valor) {
            textView.setText(getResources().getString(R.string.menor));
        }
    }
}
