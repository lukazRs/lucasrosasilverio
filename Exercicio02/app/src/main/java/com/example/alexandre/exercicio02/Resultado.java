package com.example.alexandre.exercicio02;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;

public class Resultado extends AppCompatActivity {
    TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        SharedPreferences spAposta = getSharedPreferences("spCont", MODE_PRIVATE);

        int cont = spAposta.getInt("spCont", 0);
        int valor = spAposta.getInt("spValor", 0);

        textView2 = (TextView)findViewById(R.id.textView2);

        textView2.setText(String.format("Você acertou. O resultado é %s. E o número de tentativas foi %s", String.valueOf(valor), String.valueOf(cont)));
    }
}
