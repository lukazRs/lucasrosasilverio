package com.example.alexandre.exercicio02;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    int valor;
    String digitado;
    int cont;
    EditText editText;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText)findViewById(R.id.editText);
        textView = (TextView)findViewById(R.id.textView);

        textView.setText("Digite um número: ");

        valor = new Random().nextInt(3);
    }

    public void ativarPalpite(View view) {

        digitado = editText.getText().toString();

        if(Integer.parseInt(digitado) == valor) {
            SharedPreferences spAposta = getSharedPreferences("spCont", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = spAposta.edit();
            editor.clear().apply();
            editor.putInt("spCont", cont);
            editor.putInt("spValor", Integer.parseInt(digitado));
            editor.commit();

            Intent i = new Intent(this, Resultado.class);
            startActivity(i);
        } else if(Integer.parseInt(editText.getText().toString()) < valor) {
            cont++;
            textView.setText(getResources().getString(R.string.maior));
        } else if(Integer.parseInt(editText.getText().toString()) > valor) {
            cont++;
            textView.setText(getResources().getString(R.string.menor));
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        textView.setText("Digite um número: ");
        editText.setText("");

        valor = new Random().nextInt(3);
    }
}