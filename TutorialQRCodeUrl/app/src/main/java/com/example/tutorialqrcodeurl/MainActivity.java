package com.example.tutorialqrcodeurl;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private Button btnScan;
    private TextView lblNome, lblEndereco;

    private IntentIntegrator qrScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnScan = findViewById(R.id.button);
        lblNome = findViewById(R.id.tvName);
        lblEndereco = findViewById(R.id.tvAddress);

        qrScan = new IntentIntegrator(this);

        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrScan.initiateScan();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Dados da codificação do QRCode.
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if(result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                try {
                    JSONObject obj = new JSONObject(result.getContents());
                    String address = obj.getString("address");

                    if(URLUtil.isValidUrl(address)) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(address));
                        startActivity(intent);
                    } else {
                        lblNome.setText(obj.getString("name"));
                        lblEndereco.setText(address);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    //Um QRCode diferente do esperado foi lido, apresente-o ao usuário
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
