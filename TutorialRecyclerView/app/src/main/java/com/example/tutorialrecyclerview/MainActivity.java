package com.example.tutorialrecyclerview;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.tutorialrecyclerview.adapter.LivroAdapter;

import java.util.ArrayList;

import model.Livro;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Livro> listaLivros;
    RecyclerView rview;
    static final int REQUEST = 1;
    static final int RESULT_OK = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rview = findViewById(R.id.recyclerView);
        listaLivros = new ArrayList<>();

        listaLivros.add(new Livro("O Senhor dos Anéis", "J. R. R. Tolkien",
                "Fantasia épica onde elfos, anões, hobbits e homens enfrentam os poderes do mal."));
        listaLivros.add(new Livro("Uma breve história do tempo", "Stephen W. Hawking",
                "Uma introdução a alguns conceitos mais profundos da Física"));
        listaLivros.add(new Livro("A espada da galáxia", "Marcelo Cassaro",
                "A premiada ficção científica onde alienígenas rivais resolvem suas disputas na Terra."));

        rview.setAdapter(new LivroAdapter(listaLivros, this));
        RecyclerView.LayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rview.setLayoutManager(layout);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), IncluirLivro.class);
                startActivityForResult(i, REQUEST);
            }
        });

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

            if (resultCode == 1) {
                Livro livro = (Livro)data.getParcelableExtra("livro");
                listaLivros.add(livro);
                rview.getAdapter().notifyDataSetChanged();
            }

    }


}
