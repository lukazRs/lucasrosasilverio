package com.example.tutorialrecyclerview.adapter;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;

import com.example.tutorialrecyclerview.R;

public class LivroViewHolder extends ViewHolder {
    final TextView titulo;
    final TextView autor;
    final TextView descricao;

    public LivroViewHolder(View itemView) {
        super(itemView);
        titulo = itemView.findViewById(R.id.txtTitulo);
        autor = itemView.findViewById(R.id.txtAutor);
        descricao = itemView.findViewById(R.id.txtDesc);
    }
}
