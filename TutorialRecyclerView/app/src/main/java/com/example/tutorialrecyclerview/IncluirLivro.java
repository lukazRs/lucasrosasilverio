package com.example.tutorialrecyclerview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import model.Livro;

public class IncluirLivro extends AppCompatActivity {
    EditText etNome;
    EditText etAutor;
    EditText etDescricao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incluir_livro);

        etNome = (EditText)findViewById(R.id.etNome);
        etAutor = (EditText)findViewById(R.id.etAutor);
        etDescricao = (EditText)findViewById(R.id.etDescricao);

    }

    public void incluirLivro(View view) {
        Bundle b = new Bundle();
        b.putParcelable("livro", new Livro(etNome.getText().toString(), etAutor.getText().toString(), etDescricao.getText().toString()));
        Intent i = new Intent();
        i.putExtras(b);
        setResult(1, i);
        finish();
    }
}
