package com.example.tutorialmeuslivros;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import org.w3c.dom.Text;

public class AddActivity extends AppCompatActivity {
    private EditText editTitulo;
    private EditText editAutor;
    private EditText editEditora;
    public static final String TITLE_REPLY = "com.example.android.livrosql.TITLE_REPLY";
    public static final String AUTOR_REPLY = "com.example.android.livrosql.AUTOR_REPLY";
    public static final String EDITORA_REPLY = "com.example.android.livrosql.EDITORA_REPLY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        editTitulo = findViewById(R.id.editText);
        editAutor = findViewById(R.id.editText2);
        editEditora = findViewById(R.id.editText3);
    }

    public void salvaLivro(View v) {
        Intent replyIntent = new Intent();
        if(TextUtils.isEmpty(editTitulo.getText()) ||
                TextUtils.isEmpty(editAutor.getText()) ||
                TextUtils.isEmpty(editEditora.getText())) {
            setResult(RESULT_CANCELED, replyIntent);
        } else {
            String titulo = editTitulo.getText().toString();
            String autor = editAutor.getText().toString();
            String editora = editEditora.getText().toString();
            replyIntent.putExtra(TITLE_REPLY, titulo);
            replyIntent.putExtra(AUTOR_REPLY, autor);
            replyIntent.putExtra(EDITORA_REPLY, editora);
            setResult(RESULT_OK, replyIntent);
        }
        finish();
    }
}
