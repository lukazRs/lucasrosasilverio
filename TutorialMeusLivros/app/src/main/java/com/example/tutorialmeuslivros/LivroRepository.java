package com.example.tutorialmeuslivros;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class LivroRepository {
    private LivroDAO livroDAO;
    private LiveData<List<Livro>> listaLivros;

    LivroRepository(Application application) {
        LivroRoomDatabase db = LivroRoomDatabase.getDatabase(application);
        livroDAO = db.livroDAO();
        listaLivros = livroDAO.getAllLivros();
    }

    LiveData<List<Livro>> getAllLivros() {
        return listaLivros;
    }

    public void insert(Livro livro) {
        new insertAsyncTask(livroDAO).execute(livro);
    }

    public static class insertAsyncTask extends AsyncTask<Livro, Void, Void> {
        private LivroDAO mAsyncTaskDao;

        insertAsyncTask(LivroDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Livro... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void update(Livro livro) {
        new updateAsyncTask(livroDAO).execute(livro);
    }

    public static class updateAsyncTask extends AsyncTask<Livro, Void, Void> {
        private LivroDAO mAsyncTaskDao;

        private updateAsyncTask(LivroDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Livro... params) {
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

    public void delete(Livro livro) {
        new deleteAsyncTask(livroDAO).execute(livro);
    }

    public static class deleteAsyncTask extends AsyncTask<Livro, Void, Void> {
        private LivroDAO mAsyncTaskDao;

        private deleteAsyncTask(LivroDAO dao) {mAsyncTaskDao = dao;}

        @Override
        protected Void doInBackground(Livro... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }
}
