package com.example.tutorialmeuslivros;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import static com.example.tutorialmeuslivros.MainActivity.DELETED;

public class LivroListAdapter extends RecyclerView.Adapter<LivroListAdapter.LivroViewHolder> {

    class LivroViewHolder extends RecyclerView.ViewHolder {
        private final TextView tituloItemView;
        private final TextView autorItemView;
        private final TextView editoraItemView;
        private final ImageButton editaLivro;
        private final ImageButton deletaLivro;

        private LivroViewHolder(View itemView) {
            super(itemView);
            tituloItemView = itemView.findViewById(R.id.titulo);
            autorItemView = itemView.findViewById(R.id.autor);
            editoraItemView  = itemView.findViewById(R.id.editora);
            editaLivro = itemView.findViewById(R.id.editaLivro);
            deletaLivro = itemView.findViewById(R.id.deletaLivro);
        }
    }

    private final LayoutInflater mInflater;
    private List<Livro> listaLivros;
    public Context context;

    LivroListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public LivroViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item,
                parent, false);
        return new LivroViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final LivroViewHolder holder, int position) {
        if(listaLivros != null) {
            Livro current = listaLivros.get(position);
            holder.tituloItemView.setText(current.getTitulo());
            holder.autorItemView.setText(current.getAutor());
            holder.editoraItemView.setText(current.getEditora());
        } else {
            holder.tituloItemView.setText("Sem dados");
            holder.autorItemView.setText("Sem dados");
            holder.editoraItemView.setText("Sem dados");
        }

        holder.editaLivro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listaLivros != null) {
                    Livro current = listaLivros.get(holder.getAdapterPosition());
                    Intent intent = new Intent(context, EditActivity.class);
                    intent.putExtra("livro", current);
                    ((Activity)context).startActivityForResult(intent, MainActivity.EDIT_LIVRO_ACTIVITY_REQUEST_CODE);
                }
            }
        });

        holder.deletaLivro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listaLivros != null) {
                    Livro current = listaLivros.get(holder.getAdapterPosition());

                    Intent intent = new Intent();
                    intent.putExtra("livro", current);

                    ((Activity)context).setResult(DELETED, intent);
                    ((Activity)context).finish();
                }
            }
        });
    }

    void setLivros(List<Livro> livros) {
        listaLivros = livros;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(listaLivros != null)
            return listaLivros.size();
        else return 0;
    }
}

