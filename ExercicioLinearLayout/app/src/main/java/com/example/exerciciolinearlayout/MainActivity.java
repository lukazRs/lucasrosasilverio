package com.example.exerciciolinearlayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;

import adapter.ContatoAdapter;
import model.Contato;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Contato> listaContatos;
    RecyclerView rview;
    LinearLayout layout;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        layout = new LinearLayout(this);
        layout.setPadding(10,10,10,10);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setGravity(Gravity.CENTER);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        ));

        listaContatos = new ArrayList<>();

        rview = new RecyclerView(this);
        rview.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1250));
        rview.setAdapter(new ContatoAdapter(listaContatos, this));
        RecyclerView.LayoutManager lm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rview.setLayoutManager(lm);

        layout.addView(rview);

        Button cadastrar = new Button(this);
        cadastrar.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        cadastrar.setText("CADASTRAR");
        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Cadastro.class);
                startActivityForResult(i, 1);
            }
        });
        layout.addView(cadastrar);

        setContentView(layout);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == 1) {
            Contato contato = (Contato)data.getParcelableExtra("contato");
            listaContatos.add(contato);
            rview.getAdapter().notifyDataSetChanged();
        }

    }
}
