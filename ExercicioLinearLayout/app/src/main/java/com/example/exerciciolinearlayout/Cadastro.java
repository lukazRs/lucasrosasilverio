package com.example.exerciciolinearlayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import model.Contato;

public class Cadastro extends AppCompatActivity {
    EditText tnome;
    EditText ttelefone;
    Contato contato;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Cria o Layout
        LinearLayout layout = new LinearLayout(this);
        layout.setPadding(10,10,10,10);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        ));


        TextView nome = new TextView(this);
        nome.setText("Nome: ");
        nome.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        layout.addView(nome);

        tnome = new EditText(this);
        tnome.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        layout.addView(tnome);

        tnome.requestFocus();

        TextView senha = new TextView(this);
        senha.setText("Telefone: ");
        senha.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        ));

        layout.addView(senha);

        ttelefone = new EditText(this);
        ttelefone.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        ));

        layout.addView(ttelefone);

        Button ok = new Button(this);
        ok.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        ok.setText("OK");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastrar();
            }
        });
        layout.addView(ok);

        setContentView(layout);
    }

    public void cadastrar() {
        Bundle b = new Bundle();
        b.putParcelable("contato", new Contato(tnome.getText().toString(), ttelefone.getText().toString()));
        Intent i = new Intent();
        i.putExtras(b);
        setResult(1, i);
        finish();
    }
}
