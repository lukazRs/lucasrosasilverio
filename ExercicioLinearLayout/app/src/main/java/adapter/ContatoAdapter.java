package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.exerciciolinearlayout.R;

import java.util.List;

import model.Contato;

public class ContatoAdapter extends RecyclerView.Adapter {
    private List<Contato> listaContatos;
    private Context context;

    public ContatoAdapter(List<Contato> listaContatos, Context context) {
        this.listaContatos = listaContatos;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_lista, parent, false);
        return new ContatoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ContatoViewHolder theHolder = (ContatoViewHolder)holder;
        Contato contato = listaContatos.get(position);
        theHolder.nome.setText(contato.getNome());
        theHolder.telefone.setText(contato.getTelefone());
    }

    @Override
    public int getItemCount() {
        return listaContatos.size();
    }
}
